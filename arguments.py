#Format the string s by unpacking the person dictionary.
'''

def name_age(name, age):
    s = "I am {}. I'm {} years old."
    print(s.format(name,age))
person = {'name': 'Tom', 'age': 10}
name_age(**person)
'''

#Define a function add3
'''
def add3(a,b,c):
    return a+b+c
#items=[1,2,3]
add_args = {'a': 1, 'b': 2, 'c': 3}
print(add3(**add_args))
'''
#Define a function add_n which accepts any number of arguments and returns the sum of all the arguments.
'''
def add_n(*args):
    total=0
    for num in args:
        total+=num
    return total
print(add_n(2+3+4))
'''
#Define a function that accepts any kind of arguments and keyword-arguments,
#and prints the arguments and keyword-arguments.
'''
def any_arguments(**kwargs):
    for key, value in kwargs.items():
        print("{}:{}".format(key,value))

any_arguments(First_Name='hua', Last_Name='bol', Email='hua@bol')
'''